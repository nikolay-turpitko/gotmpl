package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"plugin"
	"strings"
	"text/template"

	appplugin "gotmpl/plugin"
)

func main() {

	ex, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}
	ex = filepath.Dir(ex)
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	engines, funcs := loadPlugins(ex, wd)
	if len(engines) == 0 {
		log.Fatal("no template engines loaded")
	}
	log.Printf("loaded plugins, engines: %d, funcs: %d", len(engines), len(funcs))
	strStdin := readStdinIfNotEmpty()
	for _, engine := range engines {
		ctx := map[string]interface{}{} // template context
		if err = engine(strStdin, funcs, ctx); err == nil {
			break // engine processed input
		}
		log.Println(err)
	}
}

// readStdinIfNotEmpty reads text from stdin into string if there is some data
// in stdin. It returns empty string if no data is available.
func readStdinIfNotEmpty() string {

	fi, err := os.Stdin.Stat()
	if err != nil {
		log.Fatal(err)
	}
	if fi.Mode()&os.ModeNamedPipe == 0 && fi.Size() == 0 {
		return ""
	}
	b, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	return string(b)
}

// loadPlugins checks provided paths for possible plugins, attempts to load
// them and populates engines and funcs.
func loadPlugins(
	paths ...string) (engines []appplugin.TemplateEngineFunc, funcs template.FuncMap) {

	funcs = template.FuncMap{}
	checked := map[string]struct{}{}
	for _, path := range paths {
		log.Printf("checking path %q for plugins...", path)
		err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
			if _, ok := checked[path]; ok {
				return nil
			}
			checked[path] = struct{}{}
			if err != nil {
				log.Printf("accessing path %q: %v", path, err)
				return nil
			}
			if info.IsDir() {
				return nil
			}
			if !possiblePlugin(path) {
				return nil
			}
			newEngines, newFuncs, err := loadPlugin(path)
			if err != nil {
				log.Printf("loading plugin %q: %v", path, err)
			}
			log.Printf("loaded plugin %s, engines: %d, funcs: %d", path, len(newEngines), len(newFuncs))
			engines = append(engines, newEngines...)
			if len(newFuncs) > 0 {
				for k, v := range newFuncs {
					if k == "_" {
						k = baseFileNameWithoutExt(path)
					}
					funcs[k] = v
				}
			}
			return nil
		})
		if err != nil {
			log.Printf("loading plugins from path %q: %v", path, err)
		}
	}
	return
}

// possiblePlugin checks file path's extension to see if it can be a plugin file.
// It is used to reduce warning noise during dev tests, when plugin's folder
// can contain source code files.
func possiblePlugin(path string) bool {

	patterns := []string{
		"*.gotmpl-plugin.so",
		"*.gotmpl-plugin.so.*",
		"*.gotmpl-plugin.bundle",
	}
	base := filepath.Base(path)
	for _, p := range patterns {
		match, err := filepath.Match(p, base)
		if err != nil {
			log.Printf("matching extension %q: %v", path, err)
			continue
		}
		if match {
			return true
		}
	}
	return false
}

// loadPlugin loads plugin - populates engines and funcs.
func loadPlugin(
	path string) (engines []appplugin.TemplateEngineFunc, funcs template.FuncMap, err error) {

	log.Printf("loading plugin: %s", path)
	plug, err := plugin.Open(path)
	if err != nil {
		return
	}
	sym, err := plug.Lookup("RegisterTemplateEngines")
	if err != nil && !strings.Contains(err.Error(), "symbol RegisterTemplateEngines not found") {
		return
	}
	err = nil
	if sym != nil {
		regEngine, ok := sym.(func() []appplugin.TemplateEngineFunc)
		if !ok {
			err = fmt.Errorf("unexpected type for RegisterTemplateEngines: %T", sym)
			return
		}
		engines = regEngine()
	}
	sym, err = plug.Lookup("RegisterTemplateFunctions")
	if err != nil && !strings.Contains(err.Error(), "symbol RegisterTemplateFunctions not found") {
		return
	}
	err = nil
	if sym != nil {
		regFuncs, ok := sym.(func() template.FuncMap)
		if !ok {
			err = fmt.Errorf("unexpected type for RegisterTemplateFunctions: %T", sym)
			return
		}
		funcs = regFuncs()
	}
	return
}

// baseFileNameWithoutExt returns base filename without extension.
func baseFileNameWithoutExt(path string) string {
	base := filepath.Base(path)
	ext := filepath.Ext(base)
	l := len(ext)
	if l == 0 {
		return base
	}
	return baseFileNameWithoutExt(base[:len(base)-l])
}
