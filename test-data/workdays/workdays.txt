Work days from 2021-01-01 to 2021-01-31: {{workdays.Number (period "2021-01-01" "2021-01-31") (weekends "Fri" "Sat") (holidays "2021-01-07") }}
Or without holidays: {{workdays.Number (period "2021-01") (weekends) (holidays) }}
