# gotmpl

Go (golang) template executor, extensible with plugins.

__TL;DR__;: You may like [gomplate](https://github.com/hairyhenderson/gomplate).
Or you may borrow ideas from it. It is reach, flexible and supported.
I'd use it myself if it was possible to extend it without bloating it.

Please also notice, that Go plugins are [not currently supported on
Windows](https://golang.org/pkg/plugin/). So, this tool won't work for
Windows users, or you may try using it via
[WSL](https://docs.microsoft.com/en-us/windows/wsl/about) (I haven't tried it
myself). I tested only on Linux and Mac.

## Why yet another such tool?

- as a proof of concept;
- to experiment with Go plugins;
- for my own needs;
- because it was easier to create my own simple app, then introduce
  questionable logic into others's projects;
- to be able easily extend and customize it with throwaway pices of Go code.

Originally, I wanted a call to `icu4c` lib from inside template function.
It required to download and build a heavy library, bound with cgo, which
was more heavy, then the rest of the project. It was not an option to
introduce such a dependency into the small project, so, for years I used
my own tool, but had a nagging idea to try Go plugins as a remedy.

Idea was to make template engines and template functions plugable:

- to decouple them from the app;
- to reuse them in similar apps;
- to have simple app, executing templates without any bloat;
- to allow potential users to create their own sets of template functions
  without need to recompile app.

## CLI interface

You can run it like following:

```
echo '{{$d := exec.RunCmd "date" "+%Y-%m-%d"}}Today is {{$d}}' | gotmpl
echo '{{template "b.txt"}}' | gotmpl a.txt b.txt
cat some-template.html.tmpl | gotmpl other-template.txt.tmpl some-dir/*.html.tmpl
gotmpl first.html.tmpl *.html.tmpl
gotmpl *.html.tmpl
```

Whichever case works for you. All arguments and `stdin` are parsed by engine plugins.
Normally, first template from the list executed first (like an entry point).
One on `stdin` should have precidence, then first one from the list. You may put it
explicitly in the beginnig of the arguments list, or name it properly and
rely on the shell's order of pathname expansion.

Plugins may parse CLI flags and arguments and introduce their own logic.
Use `./gotmpl -h` to see what's currently in the box. At the time of writing,
these are available:

  - `-engine string`
    	which template engine to use (`gotext/gohtml/ace`); without this flag
    	all templates will be tried in turn, until some of them returns
    	result;

  - `-left-delim string`
    	left delimiter for template's actions;

  - `-right-delim string`
    	right delimiter for template's actions;

  - `-template-args string`
    	goes into template's context as `.template-args`; it is a string,
    	that can be parsed by the template with help of the template
    	functions; for instance, it can be JSON or YAML or comma-separated
    	key/value pairs, etc.

I tried to introduce minimum of concepts. So, I decided not to invent
datasources, configs, JSON/YAML/etc parsers and such. All this could be done
from within your custom plugins and invoked from your custom template
functions. See source code, `Makefile` and tests for examples.

Notice, that everything from `stdin` will be read into variable in the memory
before passing it to the plugins. Avoid sending long files via `stding`.

## Building and installing

```
make
```

You'll need a GNU Make and Go 1.14+ compiler installed.

`make install` creates symlinks to executable and plugin directory from the
project in the `$HOME/bin`. This will allow current user to use tool without
full path. Notice, that you may need to add `$HOME/bin` into your `$PATH` in
your `.profile` or `.bushrc` (some Linux distros have it already). You may move
actual files manually if you prefer or if want to delete project directory.

This way allows you to easily add custom sets of plugins to be discovered by
the tool - just put them to some directory and create a symbolic link (with any
name) inside `$HOME/bin`.

## Some implementation details

When programs starts it attempts to discover possible plugins on the execution
path of the program (near executable file or in the current working directory).
For simplicity, let's use `*.gotmpl.plugin.*` suffix, like
`*.gotmpl-plugin.so`, `*.gotmpl-plugin.so.1` or `*.gotmpl-plugin.bundle`.

When program finds such a plugin, it attempts to load it and invoke it's
`func RegisterTempleteEngine() []TemplateEngineFunc` or
`func RegisterTemplateFunctions() template.FuncMap` if one exists, where
`template.FuncMap` defined by the Go `text/template` package, and
`TemplateEngineFunc` is a template engine implementation.

Functions in the map are not altered and put into the same global map, so, they
can collide. This is done intentionally and allows plugins to provide concise
function names. User can remove conflicting plugins from the path. Plugin
developer may choose unique names.

Lifehack: plugin developer can expose function, which returns type with public
methods defined on it. It'll allow to use dot notation in the template like
`myplugAPI.MyFunc arg1 ... argN`. This trick can be used to create namespaces
for template functions. See included plugins for example.

Special case (primarily to use with lifehack above): function, named `_`
(single underscore), will be renamed to plugin file's base name. Notice, that
plugin file's base name should contain only symbols, allowed in function names,
for this to work properly. This trick may help user to rename file (and thus
namespace) to resolve conflicts between plugins.

After program populates template functions from all plugins, it continues to
read whatever it can from `stdin` to string variable and invokes template
engines in turn with this variable as one of arguments.

All discovered template functions are passed to the template engine. Freshly
created empty context (`map[string]interface{}`) is also passed and can be
populated and used by the template engine with assistance of template functions
to hold variables.

Once some of template engines successfully completes `TemplateEngineFunc`
(returns no error) the loop and app terminates. 

Template engine can parse CLI flags and arguments to understand if it can
process provided templates or not.

Idea is that user should be able to invoke program like this:

```
cat some-template.html.tmpl | gotmpl other-template.txt.tmpl some-dir/*.html.tmpl
```

Template engine should check all provided files and text from `stdin` in
`TemplateEngineFunc` first and return an error if it doesn't understand syntax
in `stdin` or file extensions.  It may, for instance, return an error if not
all files have extension, known by the plugin. In such case application
continues to try other template engines.

If template engine accepts inputs, it should output result to `stdout`, if not
redirected by CLI flags or provided templates, which is on discretion of the
plugin. If template engine returns no error, application exits.

Template from the `stdin` (or first template from CLI arguments, if `stdin` is
empty) should be executed first and it may call other registered templates.

There can be situation when error occurs during processing templates after
plugin accepted inputs. It may leave dirty `stdout` and another plugin may
attempt to execute with the same inputs. Output will be corrupted. Its accepted
behavior for now. Plugins should be developed in a way to avoid such
situations, or end user can remove some plugins from the path to work around
it.

I imagine, that it could be possible to use template function to put something
into context, which could be checked then by the plugin to redirect output. For
instance, template could ask to post-process output with specific tool. Plugin
could register both template function and template engine to support this.

Plugin also can check specific CLI flag, such as `-engine=go-text` or
`-engine=go-html` as a hint from user to what engine to use. It's up to plugin
implementation and can be decided later, because end user should be able to
replace plugins whenever he wants.

## What's in the box?

As I said, I built this tool for my own needs - to prepare bilingual invoices.

With it I can save invoice templates and invoices themselves in the Git
repository as a plain text and generate PDF documents from them whenever I
need.

So, I created minimal plugins set for this task (except `exec` and `sprig`,
which were added later):

- `dict`: simple dictionary;
- `exec`: allows to execute shell command;
- `math`: `Sum` and `Mul` functions for very basic math;
- `spell`: spells out integers or currency values in different languages (via
  https://github.com/moul/number-to-words; RU and EN, USD and RUB are primarily
  addressed);
- `sprig`: illustrates how to wrap existing libs of template functions into
  plugin (also a rich lib of useful functions: [sprig](https://masterminds.github.io/sprig/));
- `time`: to create (parse) time value in template from string;
- `util`: implements kind of `/dev/null` for func pipes inside template and
  function to convert string to `io.Reader`, when other functions consume it;
- `workdays`: allows to count working days within range similar to how
  spreadsheets can do it;
- `yaml`: to parse YAML strings inside template (useful to prepare
  dictionaries);

Currently no documentation besides the code.

`test-data` directory contains template examples, of which `invoice` is most elaborated.

`Makefile` contains `test` section, which can be viewed as usage examples.
