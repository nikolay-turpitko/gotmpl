module gotmpl

go 1.21.7

require (
	github.com/Masterminds/sprig v2.22.0+incompatible
	github.com/c2fo/testify v0.0.0-20150827203832-fba96363964a
	github.com/stretchr/testify v1.9.0
	github.com/yosssi/ace v0.0.5
	golang.org/x/text v0.14.0
	gopkg.in/yaml.v3 v3.0.1
	moul.io/number-to-words v0.7.0
)

require (
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/huandu/xstrings v1.4.0 // indirect
	github.com/imdario/mergo v0.3.16 // indirect
	github.com/mitchellh/copystructure v1.2.0 // indirect
	github.com/mitchellh/reflectwalk v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.21.0 // indirect
)
