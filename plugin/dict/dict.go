package main

import (
	"text/template"
)

// RegisterTemplateFunctions executed by the app to register template functions.
func RegisterTemplateFunctions() template.FuncMap {

	return template.FuncMap{
		"_": func() PluginFuncs { return impl{} },
	}
}

// PluginFuncs contains methods to be used in templates as namespased functions.
type PluginFuncs interface {

	// New creates new dictionary.
	New() Dictionary
	// Wrap wraps map as a dictionary.
	Wrap(m map[string]interface{}) Dictionary
	// Set sets entry in the dictionary.
	Set(k string, v interface{}, d Dictionary) Dictionary
	// Del deletes etry from the dictionary.
	Del(k string, d Dictionary) Dictionary
}

var _ PluginFuncs = impl{}

// impl implements PluginFuncs interface.
type impl struct{}

func (impl) New() Dictionary {

	return Dictionary{}
}

func (impl) Wrap(m map[string]interface{}) Dictionary {

	return Dictionary(m)
}

func (impl) Set(k string, v interface{}, d Dictionary) Dictionary {

	d[k] = v
	return d
}

func (impl) Del(k string, d Dictionary) Dictionary {

	delete(d, k)
	return d
}

// Dictionary is a map with string keys, containing arbitrary data.
type Dictionary map[string]interface{}

// Set sets entry in the dictionary.
func (d Dictionary) Set(k string, v interface{}) Dictionary {

	d[k] = v
	return d
}

// Del deletes etry from the dictionary.
func (d Dictionary) Del(k string) Dictionary {

	delete(d, k)
	return d
}
