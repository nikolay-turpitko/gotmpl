package plugin

import "text/template"

// gotmpl invokes couple of functions to allow plugin to register template
// enginges and/or template functions.
//
// Plugin exposes either (or both) of these functions:
//
// - RegisterTempleteEngines() []TemplateEngineFunc
// - RegisterTemplateFunctions() template.FuncMap

// RegisterTemplateEnginesFunc used to register template engines by the plugin.
type RegisterTemplateEnginesFunc func() []TemplateEngineFunc

// RegisterTemplateFunctionsFunc used to register template functions by the plugin.
type RegisterTemplateFunctionsFunc func() template.FuncMap

// TemplateEngineFunc is a template engine implementation. It must return no
// error if it was able to handle intputs and produce output. It should write
// output to stdout, but may store it in the file system or otherwise, if
// instructed with CLI flags or input templates. It must return an error if it
// doesn't understand templates for application to try other implementations.
// It's better to return such an error before plugin corrupted stdout to let
// other plugins to try.
//
// Argument stdin contains text, captured by the app from stdin.
// Argument funcs contains template functions, registered by all plugins.
// Argument ctx contains empty context. Context can be used by means of
// template functions to allow templates to hold temporary data.
type TemplateEngineFunc func(stdin string, funcs template.FuncMap, ctx map[string]interface{}) error
