package main

import (
	"strings"
	"text/template"
	"time"
)

// RegisterTemplateFunctions executed by the app to register template functions.
func RegisterTemplateFunctions() template.FuncMap {

	return template.FuncMap{

		"_": func() PluginFuncs { return impl{} },

		// holidays parses strings to dates (in "YYYY-MM-DD" format). Skips
		// unparsed dates.
		"holidays": func(d ...interface{}) []time.Time {
			return dates(ifaceToStringSlice(d)...)
		},

		// weekends parses strings to dates (at least 3 letter prefix of
		// English weekday name). Skips unknown names.
		"weekends": func(d ...interface{}) []time.Weekday {
			weekdays := []time.Weekday{}
			for _, d := range ifaceToStringSlice(d) {
				if len(d) < 2 {
					continue
				}
				d := strings.Title(strings.ToLower(d))
				for w := time.Sunday; w <= time.Saturday; w++ {
					if strings.HasPrefix(w.String(), d) {
						weekdays = append(weekdays, w)
					}
				}
			}
			return weekdays
		},

		// period returns Period. If only one string provided, it should be a
		// month in the "YYYY-MM" format. Then first and last dates of this
		// month will be the first and last dates of the Period. In other case,
		// there should be exactly 2 strings in "YYYY-MM-DD" format.
		"period": func(arg ...interface{}) Period {
			a := ifaceToStringSlice(arg)
			l := len(a)
			switch {
			case l == 1:
				t1, err := time.Parse("2006-01", a[0])
				if err != nil {
					return Period{}
				}
				t2 := t1.AddDate(0, 1, 0).AddDate(0, 0, -1)
				return Period{t1, t2}
			case l == 2:
				d := dates(a[0], a[1])
				if len(d) == 2 {
					return Period{d[0], d[1]}
				}
			}
			return Period{}
		},
	}
}

// PluginFuncs contains methods to be used in templates as namespased functions.
type PluginFuncs interface {

	// Number counts number of working days in the time interval between
	// first and last dates excluding provided weekdays and holidays.
	// Dates should be in "YYYY-MM-DD" format.
	Number(
		period Period,
		weekend []time.Weekday,
		holidays []time.Time) uint
}

// Period is a time period between First and Last dates (including them).
type Period struct {
	First, Last time.Time
}

var _ PluginFuncs = impl{}

// impl implements PluginFuncs interface.
type impl struct{}

func (impl) Number(
	period Period,
	weekend []time.Weekday,
	holidays []time.Time) uint {

	return numWorkDays(period.First, period.Last, weekend, holidays)
}

// numWorkDays calculates number of working days between first and last dates
// excluding specified weekend and holidays.
func numWorkDays(
	first, last time.Time,
	weekend []time.Weekday,
	holidays []time.Time) uint {

	if first.IsZero() || last.IsZero() {
		return 0
	}

	first = truncateDate(first)
	// take next date after last, truncated to date
	last = truncateDate(last.AddDate(0, 0, 1))
	if last.Before(first) {
		return 0
	}
	// deduplicate weekends and holidays
	wd := make(map[time.Weekday]struct{}, len(weekend))
	for _, w := range weekend {
		wd[w] = struct{}{}
	}
	hd := make(map[time.Time]struct{}, len(holidays))
	for _, d := range holidays {
		hd[truncateDate(d)] = struct{}{}
	}
	numDays := uint(last.Sub(first).Hours() / 24)
	numHolidays := uint(0)
	for d := range hd {
		if !d.Before(first) && d.Before(last) {
			if _, ok := wd[d.Weekday()]; !ok { // avoid counting it twice
				numHolidays++
			}
		}
	}
	if len(wd) == 0 {
		return numDays - numHolidays
	}
	fullWeeks := numDays / 7
	incompleteWeekFirst := first.Weekday()
	incompleteWeekLast := last.Weekday()
	numWeekendDays := fullWeeks * uint(len(wd)) // num of weekends in full weeks
	for w := range wd {
		if incompleteWeekFirst <= incompleteWeekLast {
			if w >= incompleteWeekFirst && w < incompleteWeekLast {
				numWeekendDays++
			}
			continue
		}
		if (w >= incompleteWeekFirst && w <= time.Saturday) ||
			(w < incompleteWeekLast) {
			numWeekendDays++
		}
	}

	return numDays - numHolidays - numWeekendDays
}

func truncateDate(t time.Time) time.Time {
	y, m, d := t.UTC().Date()
	return time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
}

func dates(d ...string) []time.Time {
	dates := make([]time.Time, 0, len(d))
	for _, s := range d {
		ss := strings.Split(s, "T")
		if len(ss) < 2 {
			s += "T00:00:00Z"
		}
		tm, err := time.Parse(time.RFC3339, s)
		if err == nil {
			dates = append(dates, tm)
		}
	}
	return dates
}

func ifaceToStringSlice(a []interface{}) []string {
	if a == nil {
		return nil
	}
	switch len(a) {
	case 0:
		return nil
	case 1:
		switch a := a[0].(type) {
		case nil:
			return nil
		case string:
			return []string{a}
		case []string:
			return a
		case []interface{}:
			return ifaceToStringSlice(a)
		}
	}
	s := make([]string, 0, len(a))
	for _, a := range a {
		s = append(s, a.(string))
	}
	return s
}
