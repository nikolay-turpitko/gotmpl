package main

import (
	"math/rand"
	"strings"
	"testing"
	"time"

	"github.com/c2fo/testify/assert"
	"github.com/c2fo/testify/require"
)

func TestNumWorkDays(t *testing.T) {
	expandDateString := func(s string) string {
		ss := strings.Split(s, "T")
		if len(ss) < 2 {
			s += "T00:00:00Z"
		}
		return s
	}
	mustParse := func(s string) time.Time {
		s = expandDateString(s)
		tm, err := time.Parse(time.RFC3339, s)
		require.NoError(t, err)
		return tm
	}
	type fixture struct {
		from, to time.Time
		weekend  []time.Weekday
		holidays []time.Time
		expected uint
	}
	makeFixture := func(expected uint, args ...string) fixture {
		weekdays := []time.Weekday{}
		holidays := []time.Time{}
		for _, arg := range args[2:] {
			if tm, err := time.Parse(time.RFC3339, expandDateString(arg)); err == nil {
				holidays = append(holidays, tm)
				continue
			}
			for w := time.Sunday; w <= time.Saturday; w++ {
				if strings.HasPrefix(w.String(), arg) {
					weekdays = append(weekdays, w)
				}
			}
		}
		return fixture{
			mustParse(args[0]),
			mustParse(args[1]),
			weekdays,
			holidays,
			expected,
		}
	}
	// manually calculated expectations
	fixtures := []fixture{
		makeFixture(0, "2020-12-10", "2020-12-01"),
		makeFixture(1, "2020-12-01", "2020-12-01"),
		makeFixture(2, "2020-12-01", "2020-12-02"),
		makeFixture(31, "2020-12-01", "2020-12-31"),
		makeFixture(23, "2020-12-01", "2020-12-31", "Fri", "Sat", "Fri", "Sat"),
		makeFixture(23, "2020-12-01", "2020-12-31", "Fri", "Sat"),
		makeFixture(22, "2020-12-01", "2020-12-31", "Fri", "Sat", "2020-12-27"),
		makeFixture(30, "2020-12-01", "2020-12-31", "2020-12-27"),
		makeFixture(43, "2020-12-01", "2021-01-31", "Fri", "Sat", "2020-12-27"),
		makeFixture(23, "2021-08-26", "2021-09-27", "Fri", "Mon"),
		makeFixture(44, "2021-02-16", "2021-03-31"),
		makeFixture(127, "2021-02-16", "2021-06-24", "2021-05-23", "2021-04-21", "2021-08-09"),
		makeFixture(137, "2021-04-16", "2021-09-01", "2021-04-23", "2021-08-29"),
		makeFixture(82, "2021-04-20", "2021-07-24", "Wed"),
		makeFixture(81, "2021-04-20", "2021-07-24", "Wed", "2021-06-09", "2021-07-07", "2021-04-25", "2021-06-16"),
		makeFixture(12, "2021-11-27", "2021-12-15", "Sun", "Tue", "2021-02-28", "2021-02-01", "2021-11-29", "2021-11-29", "2021-09-08"),
		makeFixture(30, "2021-09-06", "2021-10-19", "Sun", "Tue", "2021-09-06"),
		makeFixture(40, "2021-06-26", "2021-08-21", "Fri", "Mon", "2021-11-12", "2021-05-11", "2021-05-28", "2021-06-27", "2021-09-04", "2021-09-08"),
		makeFixture(124, "2021-01-31", "2021-06-08", "2020-12-21", "2021-02-05", "2021-07-01", "2021-04-29", "2021-05-15", "2021-03-20", "2021-02-06"),

		makeFixture(74, "2021-06-05", "2021-08-19", "2021-02-08", "2021-08-19", "2021-07-25", "2021-05-12"),

		makeFixture(68, "2021-06-08", "2021-08-27", "Tue", "2020-12-30", "2021-05-14", "2021-05-21", "2021-06-09", "2021-08-28"),
		makeFixture(153, "2020-12-24", "2021-05-25", "2021-05-26"),
	}
	for _, f := range fixtures {
		//actual := numWorkDaysDumb(f.from, f.to, f.weekend, f.holidays)
		actual := numWorkDays(f.from, f.to, f.weekend, f.holidays)
		assert.Equal(t, f.expected, actual, "expected: %d, actual: %d", f.expected, actual)
	}
	// calculate expectations with dumb ceck function and number of random conditions
	for i := 0; i < 10000; i++ {
		now := time.Now()
		dates := randomDates(now, now.AddDate(5, 0, 0))
		f := fixture{dates[0], dates[1], randomWeekend(), dates[2:], 0}
		f.expected = numWorkDaysDumb(f.from, f.to, f.weekend, f.holidays)
		actual := numWorkDays(f.from, f.to, f.weekend, f.holidays)
		assert.Equal(
			t, f.expected, actual,
			"expected: %d, actual: %d\nfrom: %sto: %sweekend: %v, holidays: %s",
			f.expected, actual, formatDates(f.from), formatDates(f.to), f.weekend, formatDates(f.holidays...))
	}
}

// numWorkDaysDumb calculates working days testing conditions in the loop over
// all days between 2 dates. It is used to check correctness of numWorkDays.
func numWorkDaysDumb(
	first, last time.Time,
	weekend []time.Weekday,
	holidays []time.Time) uint {

	first = truncateDate(first)
	last = truncateDate(last.AddDate(0, 0, 1))
	n := uint(0)
DAYS:
	for d := first; d.Before(last); d = d.AddDate(0, 0, 1) {
		for _, w := range weekend {
			if w == d.Weekday() {
				continue DAYS
			}
		}
		for _, h := range holidays {
			h = truncateDate(h)
			if h.Equal(d) {
				continue DAYS
			}
		}
		n++
	}
	return n
}

// randomDates generates from 2 to 20 random dates within interval.
func randomDates(from, to time.Time) (result []time.Time) {
	rand.Seed(time.Now().UnixNano())
	n := 2 + rand.Intn(19)
	sec := to.Sub(from).Nanoseconds() / 1e9
	result = make([]time.Time, 0, n)
	for i := 0; i < n; i++ {
		t := time.Unix(from.Unix()+rand.Int63n(sec), 0)
		result = append(result, t)
	}
	return
}

// randomWeekend generates from 0 to 7 random week days.
func randomWeekend() (result []time.Weekday) {
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(8)
	result = make([]time.Weekday, 0, n)
	for i := 0; i < n; i++ {
		result = append(result, time.Weekday(rand.Intn(7)))
	}
	return
}

func formatDates(t ...time.Time) string {
	b := []byte{}
	for _, t := range t {
		b = t.AppendFormat(b, "2006-01-02; ")
	}
	return string(b)
}
