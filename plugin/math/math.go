package main

import (
	"log"
	"math"
	"text/template"
)

// RegisterTemplateFunctions executed by the app to register template functions.
func RegisterTemplateFunctions() template.FuncMap {

	return template.FuncMap{
		"_": func() PluginFuncs { return impl{} },
	}
}

// PluginFuncs contains methods to be used in templates as namespased functions.
type PluginFuncs interface {

	// Sum sums up numbers.
	Sum(...interface{}) float64
	// Mul multiplies numbers.
	Mul(...interface{}) float64
	// Round
	Round(interface{}) float64
}

var _ PluginFuncs = impl{}

// impl implements PluginFuncs interface.
type impl struct{}

func (impl) Sum(a ...interface{}) float64 {
	f := toFloat(a...)
	if len(f) == 0 {
		return 0
	}
	s := f[0]
	if len(f) == 1 {
		return s
	}
	for _, a := range f[1:] {
		s += a
	}
	return s
}

func (impl) Mul(a ...interface{}) float64 {
	f := toFloat(a...)
	if len(f) == 0 {
		return 0
	}
	m := f[0]
	if len(f) == 1 {
		return m
	}
	for _, a := range f[1:] {
		m *= a
	}
	return m
}

func (impl) Round(a interface{}) float64 {
	f := toFloat(a)
	if len(f) == 0 {
		return 0
	}
	return math.Round(f[0])
}

func toFloat(a ...interface{}) []float64 {
	f := make([]float64, 0, len(a))
	for _, a := range a {
		switch a := a.(type) {
		case float64:
			f = append(f, a)
		case int:
			f = append(f, float64(a))
		case uint:
			f = append(f, float64(a))
		default:
			log.Printf("not converted type: %T", a)
		}
	}
	return f
}
