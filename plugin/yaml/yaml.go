package main

import (
	"text/template"

	"gopkg.in/yaml.v3"
)

// RegisterTemplateFunctions executed by the app to register template functions.
func RegisterTemplateFunctions() template.FuncMap {

	return template.FuncMap{
		"_": func() PluginFuncs { return impl{} },
	}
}

// PluginFuncs contains methods to be used in templates as namespased functions.
type PluginFuncs interface {

	// ParseString parses yaml string and unmarshals it to generic collection.
	ParseString(s string) (interface{}, error)
}

var _ PluginFuncs = impl{}

// impl implements PluginFuncs interface.
type impl struct{}

func (impl) ParseString(s string) (i interface{}, err error) {

	err = yaml.Unmarshal([]byte(s), &i)
	return
}
