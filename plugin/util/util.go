package main

import (
	"io"
	"strings"
	"text/template"
)

// RegisterTemplateFunctions executed by the app to register template functions.
func RegisterTemplateFunctions() template.FuncMap {

	return template.FuncMap{
		// silent consumes intput and returns empty string.
		"silent": func(interface{}) string { return "" },
		// strreader makes io.Reader from the string.
		"strreader": func(s string) io.Reader { return strings.NewReader(s) },
	}
}
