package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	htmltemplate "html/template"
	texttemplate "text/template"

	acetemplate "github.com/yosssi/ace"

	"gotmpl/plugin"
)

// RegisterTemplateEngines executed by the app to register template engines.
func RegisterTemplateEngines() []plugin.TemplateEngineFunc {

	return []plugin.TemplateEngineFunc{gotext, gohtml, ace}
}

func gotext(
	stdin string,
	funcs texttemplate.FuncMap,
	ctx map[string]interface{}) (err error) {

	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("gotext panic: %v", r)
		}
	}()
	args, err := parseArgs("gotext")
	if err != nil {
		return fmt.Errorf("gotext parse arguments: %w", err)
	}
	if stdin == "" && len(args) == 0 {
		return fmt.Errorf("gotext no templates found")
	}
	tmpl, err := texttemplate.New("main").
		Funcs(funcs).
		Delims(leftDelim, rightDelim).
		Parse(stdin)
	if err != nil {
		return fmt.Errorf("gotext parse stdin template: %w", err)
	}
	if len(args) > 0 {
		tmpl, err = tmpl.ParseFiles(args...)
		if err != nil {
			return fmt.Errorf("gotext parse template files: %w", err)
		}
	}
	mainTmpl := "main"
	if stdin == "" {
		mainTmpl = filepath.Base(args[0])
	}
	ctx["template-args"] = templateArgs
	err = tmpl.ExecuteTemplate(os.Stdout, mainTmpl, ctx)
	if err != nil {
		return fmt.Errorf("gotext execute templates: %w", err)
	}
	return nil
}

func gohtml(
	stdin string,
	funcs texttemplate.FuncMap,
	ctx map[string]interface{}) (err error) {

	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("gohtml panic: %v", r)
		}
	}()
	args, err := parseArgs("gohtml")
	if err != nil {
		return fmt.Errorf("gohtml parse arguments: %w", err)
	}
	if stdin == "" && len(args) == 0 {
		return fmt.Errorf("gohtml no templates found")
	}
	tmpl, err := htmltemplate.New("main").
		Funcs(htmltemplate.FuncMap(funcs)).
		Delims(leftDelim, rightDelim).
		Parse(stdin)
	if err != nil {
		return fmt.Errorf("gohtml parse stdin template: %w", err)
	}
	if len(args) > 0 {
		tmpl, err = tmpl.ParseFiles(args...)
		if err != nil {
			return fmt.Errorf("gohtml parse template files: %w", err)
		}
	}
	mainTmpl := "main"
	if stdin == "" {
		mainTmpl = filepath.Base(args[0])
	}
	ctx["template-args"] = templateArgs
	err = tmpl.ExecuteTemplate(os.Stdout, mainTmpl, ctx)
	if err != nil {
		return fmt.Errorf("gohtml execute templates: %w", err)
	}
	return nil
}

func ace(
	stdin string,
	funcs texttemplate.FuncMap,
	ctx map[string]interface{}) (err error) {

	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("ace panic: %v", r)
		}
	}()
	//TODO: not tested, doesn't use stdin, uses only single first arg, which
	//should be a path to the ace template file.
	args, err := parseArgs("ace")
	if err != nil {
		return fmt.Errorf("ace parse arguments: %w", err)
	}
	if len(args) == 0 {
		return fmt.Errorf("ace no templates found")
	}
	tmpl, err := acetemplate.Load(args[0], "", &acetemplate.Options{
		FuncMap: htmltemplate.FuncMap(funcs),
	})
	if err != nil {
		return fmt.Errorf("ace load templates: %w", err)
	}
	ctx["template-args"] = templateArgs
	err = tmpl.Execute(os.Stdout, ctx)
	if err != nil {
		return fmt.Errorf("ace execute templates: %w", err)
	}
	return nil
}

var (
	leftDelim, rightDelim, templateArgs string
)

func parseArgs(name string) ([]string, error) {

	fs := flag.NewFlagSet(os.Args[0]+": "+name, flag.ContinueOnError)
	engine := ""
	fs.StringVar(&engine, "engine", "", "template engine to use")
	fs.StringVar(&leftDelim, "left-delim", "", "left delimiter for template's actions")
	fs.StringVar(&rightDelim, "right-delim", "", "right delimiter for template's actions")
	fs.StringVar(&templateArgs, "template-args", "", "goes into template's context as .template-args")
	err := fs.Parse(os.Args[1:])
	if err != nil {
		return nil, err
	}
	if engine != "" && engine != name {
		return nil, fmt.Errorf("not supported engine: %s", engine)
	}
	return fs.Args(), nil
}
