package main

import (
	"text/template"

	"github.com/Masterminds/sprig"
)

// RegisterTemplateFunctions executed by the app to register template functions.
func RegisterTemplateFunctions() template.FuncMap {

	r := sprig.TxtFuncMap()

	// rename "dict" due name conflict with predefined plugin
	r["sprig_dict"] = r["dict"]
	delete(r, "dict")

	return r
}
