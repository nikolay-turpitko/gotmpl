package main

import (
	"io"
	"os/exec"
	"text/template"
)

// RegisterTemplateFunctions executed by the app to register template functions.
func RegisterTemplateFunctions() template.FuncMap {

	return template.FuncMap{
		"_": func() PluginFuncs { return impl{} },
	}
}

// PluginFuncs contains methods to be used in templates as namespased functions.
type PluginFuncs interface {

	// RunCmd executes shell command with provided arguments and returns it's
	// output to template as string. If last argument is io.Reader, it treated as
	// stdin of the command.
	RunCmd(cmd string, args ...interface{}) (string, error)
}

var _ PluginFuncs = impl{}

// impl implements PluginFuncs interface.
type impl struct{}

func (impl) RunCmd(cmd string, args ...interface{}) (string, error) {
	var stdin io.Reader
	l := len(args)
	if l > 0 {
		last := args[l-1]
		if r, ok := last.(io.Reader); ok {
			stdin = r
			args = args[:l-1]
		}
	}
	s := make([]string, 0, len(args))
	for _, a := range args {
		s = append(s, a.(string))
	}
	c := exec.Command(cmd, s...)
	if stdin != nil {
		c.Stdin = stdin
	}
	b, err := c.Output()
	return string(b), err
}
