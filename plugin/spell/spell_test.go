package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
)

func TestSpellNumberUS(t *testing.T) {
	assert := assert.New(t)

	actual := spellNumber(142, language.AmericanEnglish)
	assert.Equal("one hundred forty-two", actual)
}

func TestSpellNumberRU(t *testing.T) {
	assert := assert.New(t)

	actual := spellNumber(142, language.Russian)
	assert.Equal("сто сорок два", actual)
}

func TestSpellMoneyUS(t *testing.T) {
	assert := assert.New(t)

	actual := spellMoney(1977.22, currency.USD, language.AmericanEnglish)
	assert.Equal("One thousand nine hundred seventy-seven US Dollars 22 cents", actual)

	actual = spellMoney(1977.22, currency.RUB, language.AmericanEnglish)
	assert.Equal("One thousand nine hundred seventy-seven Russian Rubles 22 kopecks", actual)

	actual = spellMoney(42.01, currency.USD, language.AmericanEnglish)
	assert.Equal("Forty-two US Dollars 01 cent", actual)

	actual = spellMoney(42.01, currency.RUB, language.AmericanEnglish)
	assert.Equal("Forty-two Russian Rubles 01 kopeck", actual)

	actual = spellMoney(41.01, currency.USD, language.AmericanEnglish)
	assert.Equal("Forty-one US Dollars 01 cent", actual)

	actual = spellMoney(41.01, currency.RUB, language.AmericanEnglish)
	assert.Equal("Forty-one Russian Rubles 01 kopeck", actual)

	actual = spellMoney(41, currency.USD, language.AmericanEnglish)
	assert.Equal("Forty-one US Dollars", actual)

	actual = spellMoney(41, currency.RUB, language.AmericanEnglish)
	assert.Equal("Forty-one Russian Rubles", actual)
}

func TestSpellMoneyRU(t *testing.T) {
	assert := assert.New(t)

	actual := spellMoney(1977.22, currency.USD, language.Russian)
	assert.Equal("Одна тысяча девятьсот семьдесят семь долларов США 22 цента", actual)

	actual = spellMoney(1977.22, currency.RUB, language.Russian)
	assert.Equal("Одна тысяча девятьсот семьдесят семь рублей 22 копейки", actual)

	actual = spellMoney(42.01, currency.USD, language.Russian)
	assert.Equal("Сорок два доллара США 01 цент", actual)

	actual = spellMoney(42.01, currency.RUB, language.Russian)
	assert.Equal("Сорок два рубля 01 копейка", actual)

	actual = spellMoney(41.01, currency.USD, language.Russian)
	assert.Equal("Сорок один доллар США 01 цент", actual)

	actual = spellMoney(41.01, currency.RUB, language.Russian)
	assert.Equal("Сорок один рубль 01 копейка", actual)

	actual = spellMoney(41, currency.USD, language.Russian)
	assert.Equal("Сорок один доллар США", actual)

	actual = spellMoney(41, currency.RUB, language.Russian)
	assert.Equal("Сорок один рубль", actual)
}

func TestSpellMoneyUnsupported(t *testing.T) {
	assert := assert.New(t)

	actual := spellMoney(1977.22, currency.EUR, language.Russian)
	assert.Equal("Одна тысяча девятьсот семьдесят семь, 22/100 EUR", actual)

	actual = spellMoney(1977.22, currency.EUR, language.French)
	assert.Equal("Mille neuf cent soixante-dix-sept, 22/100 EUR", actual)

	actual = spellMoney(1977.22, currency.AUD, language.Und)
	assert.Equal("1977.22 AUD", actual)

	actual = spellMoney(1977.22, currency.CNY, language.AmericanEnglish)
	assert.Equal("One thousand nine hundred seventy-seven, 22/100 CNY", actual)

	actual = spellMoney(42.01, currency.CNY, language.AmericanEnglish)
	assert.Equal("Forty-two, 1/100 CNY", actual)

	actual = spellMoney(1977.22, currency.CNY, language.Russian)
	assert.Equal("Одна тысяча девятьсот семьдесят семь, 22/100 CNY", actual)

	actual = spellMoney(42.01, currency.CNY, language.Russian)
	assert.Equal("Сорок два, 1/100 CNY", actual)

	actual = spellMoney(42, currency.CNY, language.Russian)
	assert.Equal("Сорок два CNY", actual)
}
