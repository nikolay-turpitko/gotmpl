package main

import (
	"fmt"
	"math"
	"strings"
	"text/template"
	"unicode"
	"unicode/utf8"

	"golang.org/x/text/currency"
	"golang.org/x/text/feature/plural"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/message/catalog"
	ntw "moul.io/number-to-words"
)

// RegisterTemplateFunctions executed by the app to register template functions.
func RegisterTemplateFunctions() template.FuncMap {

	return template.FuncMap{
		"_": func() PluginFuncs { return impl{} },
	}
}

// PluginFuncs contains methods to be used in templates as namespased functions.
type PluginFuncs interface {

	// Integer spells integer.
	Integer(lang string, n int) string
	// Money spells money/currency.
	Money(lang, currency string, n float64) string
}

var _ PluginFuncs = impl{}

// impl implements PluginFuncs interface.
type impl struct{}

func (impl) Integer(lang string, n int) string {
	l, err := language.Parse(lang)
	if err != nil {
		return fmt.Sprintf("%d", n)
	}
	return spellNumber(n, l)
}

func (impl) Money(lang, cur string, n float64) string {
	l, err := language.Parse(lang)
	if err != nil {
		return fmt.Sprintf("%.02f %s", n, cur)
	}
	c, err := currency.ParseISO(cur)
	if err != nil {
		return fmt.Sprintf("%.02f %s", n, cur)
	}
	return spellMoney(n, c, l)
}

var (
	spellMoneyCatalogBuilder *catalog.Builder
	spellCurrencies          = map[currency.Unit]struct{}{
		currency.USD: struct{}{},
		currency.RUB: struct{}{},
	}
)

func init() {
	b := catalog.NewBuilder()
	b.SetString(language.English, "%d %s USD", "%[2]s ${usd(1)}")
	b.SetString(language.English, "%d %s RUB", "%[2]s ${rub(1)}")
	b.SetString(language.Russian, "%d %s USD", "%[2]s ${usd(1)}")
	b.SetString(language.Russian, "%d %s RUB", "%[2]s ${rub(1)}")
	b.SetString(language.English, "%d %s USD %02d", "%[2]s ${usd(1)} %02[3]d ${cent(3)}")
	b.SetString(language.English, "%d %s RUB %02d", "%[2]s ${rub(1)} %02[3]d ${kop(3)}")
	b.SetString(language.Russian, "%d %s USD %02d", "%[2]s ${usd(1)} %02[3]d ${cent(3)}")
	b.SetString(language.Russian, "%d %s RUB %02d", "%[2]s ${rub(1)} %02[3]d ${kop(3)}")

	b.SetMacro(language.English, "usd",
		plural.Selectf(1, "",
			plural.One, "US Dollar",
			plural.Other, "US Dollars"))
	b.SetMacro(language.English, "rub",
		plural.Selectf(1, "",
			plural.One, "Russian Ruble",
			plural.Other, "Russian Rubles"))
	b.SetMacro(language.English, "cent",
		plural.Selectf(1, "",
			plural.One, "cent",
			plural.Other, "cents"))
	b.SetMacro(language.English, "kop",
		plural.Selectf(1, "",
			plural.One, "kopeck",
			plural.Other, "kopecks"))

	b.SetMacro(language.Russian, "usd",
		plural.Selectf(1, "",
			plural.One, "доллар США",
			plural.Few, "доллара США",
			plural.Other, "долларов США"))
	b.SetMacro(language.Russian, "rub",
		plural.Selectf(1, "",
			plural.One, "рубль",
			plural.Few, "рубля",
			plural.Other, "рублей"))
	b.SetMacro(language.Russian, "cent",
		plural.Selectf(1, "",
			plural.One, "цент",
			plural.Few, "цента",
			plural.Other, "центов"))
	b.SetMacro(language.Russian, "kop",
		plural.Selectf(1, "",
			plural.One, "копейка",
			plural.Few, "копейки",
			plural.Other, "копеек"))

	spellMoneyCatalogBuilder = b
}

func spellNumber(n int, lang language.Tag) string {
	lng := ntw.Languages.Lookup(strings.ToLower(lang.String()))
	if lng == nil {
		return ""
	}
	return lng.IntegerToWords(n)
}

func spellMoney(n float64, cur currency.Unit, lang language.Tag) string {
	i, f := math.Modf(n)
	ii := int(i)
	major := spellNumber(ii, lang)
	if major == "" {
		return fmt.Sprintf("%.02f %s", n, cur.String())
	}
	minor := int(math.Trunc(100*f + 0.5))
	_, _, conf := spellMoneyCatalogBuilder.Matcher().Match(lang)
	if conf == language.High || conf == language.Exact {
		if _, ok := spellCurrencies[cur]; ok {
			p := message.NewPrinter(lang, message.Catalog(spellMoneyCatalogBuilder))
			if minor == 0 {
				return upperFirst(p.Sprintf("%d %s "+cur.String(), ii, major))
			}
			return upperFirst(p.Sprintf("%d %s "+cur.String()+" %02d", ii, major, minor))
		}
	}
	if minor == 0 {
		return upperFirst(fmt.Sprintf("%s %s", major, cur.String()))
	}
	return upperFirst(fmt.Sprintf("%s, %d/100 %s", major, minor, cur.String()))
}

func upperFirst(s string) string {
	if s == "" {
		return ""
	}
	r, n := utf8.DecodeRuneInString(s)
	return string(unicode.ToUpper(r)) + s[n:]
}
