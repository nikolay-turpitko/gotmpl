package main

import (
	"text/template"
	"time"
)

// RegisterTemplateFunctions executed by the app to register template functions.
func RegisterTemplateFunctions() template.FuncMap {

	return template.FuncMap{
		"_": func() PluginFuncs { return impl{} },
	}
}

// PluginFuncs contains methods to be used in templates as namespased functions.
type PluginFuncs interface {

	// Parse parses time.
	Parse(layout, value string) (time.Time, error)
}

var _ PluginFuncs = impl{}

// impl implements PluginFuncs interface.
type impl struct{}

func (impl) Parse(layout, value string) (time.Time, error) {
	return time.Parse(layout, value)
}
