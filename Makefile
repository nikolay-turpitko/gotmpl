.PHONY: \
	all \
	clean \
	build \
	test \
	install \
	.start-build 

all: clean build test install

PLUG_NAMES = $(sort $(notdir $(basename $(subst _test, , $(wildcard ./plugin/**/*)))))
PLUGS = $(addprefix bin/, $(addsuffix .gotmpl-plugin.so, $(PLUG_NAMES)))

clean:
	@echo "Cleaning binaries..."
	@-rm -rf ./bin ./cmd/cmd 2>/dev/null || true
	@echo "Done."

build: | .start-build bin/gotmpl $(PLUGS)
	@echo "Done."

.start-build:
	@echo "Building..."

bin/gotmpl: cmd
	go build -o ./bin/gotmpl ./cmd/gotmpl.go

.SECONDEXPANSION:
$(PLUGS): %.gotmpl-plugin.so: plugin/$$(basename $$(notdir %))
	go build -buildmode=plugin -o ./$@ ./$^

# Remove "2>/dev/null" to see gotmpl debug logs (plugin and functions found, errors, etc).
test:
	@echo "Testing..."
	go test ./...
	@echo "Testing..."
	./bin/gotmpl -engine=gotext ./test-data/first/*.txt 2>/dev/null | diff -B -a -s - ./test-data/first/a.txt
	@echo "Testing..."
	echo '{{template "b.txt"}}' | ./bin/gotmpl -engine=gotext ./test-data/first/*.txt 2>/dev/null | diff -B -a -s - ./test-data/first/b.txt
	@echo "Testing..."
	echo zz | ./bin/gotmpl -engine=gohtml 2>/dev/null | diff -B -a -s - ./test-data/expected/zz.txt
	@echo "Testing..."
	echo '{{ $$v := yaml.ParseString "{xxx: yyy, yyy: zzz}" }}{{$$v.xxx}}' | ./bin/gotmpl 2>/dev/null | diff -B -a -s - ./test-data/expected/yyy.txt
	@echo "Testing..."
	./bin/gotmpl ./test-data/dict/*.txt 2>/dev/null | diff -B -a -s - ./test-data/expected/dict.txt
	@echo "Testing..."
	./bin/gotmpl ./test-data/spell/*.txt 2>/dev/null | diff -B -a -s - ./test-data/expected/spell.txt
	@echo "Testing..."
	./bin/gotmpl ./test-data/workdays/*.txt 2>/dev/null | diff -B -a -s - ./test-data/expected/workdays.txt
	@echo "Testing..."
	./bin/gotmpl test-data/invoice/invoice-2021-02-01.txt test-data/invoice/*.txt 2>/dev/null | diff -B -a -s - ./test-data/expected/invoice-1.txt
	@echo "Testing..."
	./bin/gotmpl -left-delim="<%" -right-delim="%>" -template-args='{as-of: "2021-03-01", period: "2021-02"}' test-data/invoice/regular-invoice.tmpl 2>/dev/null | ./bin/gotmpl test-data/invoice/*.txt 2>/dev/null | diff -B -a -s - ./test-data/expected/invoice-2.txt
	@echo "Testing..."
	echo '{{exec.RunCmd "/usr/bin/wc" (strreader "Test test test")}}' | ./bin/gotmpl 2>/dev/null | diff -w -B -a -s - ./test-data/expected/wc.txt
	@echo "Testing..."
	./bin/gotmpl -engine=gotext ./test-data/sprig/*.txt 2>/dev/null | diff -B -a -s - ./test-data/expected/sprig.txt
	@echo "Done."

install:
	@echo "Installing (for current user only)..."
	@mkdir -p ${HOME}/bin/gotmpl-plugins
	@ln -fs $(PWD)/bin/gotmpl ${HOME}/bin
	@ln -fs $(PWD)/bin/*.so ${HOME}/bin/gotmpl-plugins
	@echo
	@echo "Symbolic links created:"
	@ls -l ${HOME}/bin/gotmpl*
	@echo
	@echo "This lets you to execute app without specifing full path."
	@echo "You may need to add home bin directory into PATH environment variable in your .profile or .bashrc (some Linux distros do it automatically)."
	@echo "Notice: if you'll delete project's directory, you'll have only broken symbolic links. Copy files manually, if you prefer."
